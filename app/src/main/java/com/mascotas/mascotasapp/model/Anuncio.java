package com.mascotas.mascotasapp.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "anuncio")
public class Anuncio implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "codigo")
    private int codigo;

    @ColumnInfo(name = "nombre")
    private String nombre;

    @ColumnInfo(name = "raza")
    private String raza;

    @ColumnInfo(name = "edad")
    private int edad;

    @ColumnInfo(name = "descripcion")
    private String descripcion;

    @ColumnInfo(name = "especie")
    private String especie;

    @ColumnInfo(name = "imagen")
    private String imagen;

    @ColumnInfo(name = "usuario")
    private int usuario;

    @ColumnInfo(name = "adoptado")
    private int adoptado;

    @ColumnInfo(name = "contacto")
    private String contacto;

    @ColumnInfo(name = "fecPublicacion")
    private String fecPublicacion;

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRaza() {
        return raza;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getEspecie() {
        return especie;
    }

    public void setEspecie(String especie) {
        this.especie = especie;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public int getUsuario() {
        return usuario;
    }

    public void setUsuario(int usuario) {
        this.usuario = usuario;
    }

    public int getAdoptado() {
        return adoptado;
    }

    public void setAdoptado(int adoptado) {
        this.adoptado = adoptado;
    }

    public String getContacto() {
        return contacto;
    }

    public void setContacto(String contacto) {
        this.contacto = contacto;
    }

    public String getFecPublicacion() {
        return fecPublicacion;
    }

    public void setFecPublicacion(String fecPublicacion) {
        this.fecPublicacion = fecPublicacion;
    }
}
