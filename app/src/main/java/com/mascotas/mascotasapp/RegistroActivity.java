package com.mascotas.mascotasapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.mascotas.mascotasapp.data.dao.UsuarioDao;
import com.mascotas.mascotasapp.data.database.AppDatabase;
import com.mascotas.mascotasapp.databinding.ActivityRegistrarBinding;
import com.mascotas.mascotasapp.model.Usuario;

public class RegistroActivity extends AppCompatActivity {

    private ActivityRegistrarBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityRegistrarBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        inicializar();
        registrar();
    }

    private void inicializar() {

        binding.regBtnAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }


    private void registrar() {

        binding.regBtnRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String nombres = binding.regLblNombre.getText().toString();
                String apellidos = binding.regLblApellido.getText().toString();
                String direccion = binding.regLblDireccion.getText().toString();
                String telefono = binding.regLblTelefono.getText().toString();
                String correo = binding.regLblCorreo.getText().toString();
                String contrasenia = binding.regLblContrasenia.getText().toString();
                String repContrasenia = binding.regLblConfcontrasenia.getText().toString();

                if(nombres.isEmpty()){
                    mostrarToast("Debe ingresar los nombres");
                    return;
                }
                if(apellidos.isEmpty()){
                    mostrarToast("Debe ingresar los apellidos");
                    return;
                }
                if(direccion.isEmpty()){
                    mostrarToast("Debe ingresar la direccion");
                    return;
                }
                if(telefono.isEmpty()){
                    mostrarToast("Debe ingresar el telefono");
                    return;
                }
                if(correo.isEmpty()){
                    mostrarToast("Debe ingresar el correo");
                    return;
                }
                if(contrasenia.isEmpty()){
                    mostrarToast("Debe ingresar la contraseña");
                    return;
                }
                if(repContrasenia.isEmpty()){
                    mostrarToast("Debe ingresar la contraseña");
                    return;
                }
                if(!contrasenia.equals(repContrasenia)){
                    mostrarToast("Las contraseñas deben coincidir");
                    return;
                }

                if(!binding.regChkTerminos.isChecked()){
                    mostrarToast("Debe aceptar los terminos y condiciones");
                    return;
                }

                AppDatabase database = Room.databaseBuilder(RegistroActivity.this, AppDatabase.class, "db-mascota")
                        .allowMainThreadQueries()
                        .build();

                UsuarioDao usuarioDao = database.getUsuarioDAO();

                Usuario usuarioDuplicado = usuarioDao.verificarDuplicado(correo);

                if (usuarioDuplicado==null){

                    Usuario usuario = new Usuario();
                    usuario.setNombres(nombres);
                    usuario.setApellidos(apellidos);
                    usuario.setDireccion(direccion);
                    usuario.setCorreo(correo);
                    usuario.setTelefono(telefono);
                    usuario.setContrasenia(contrasenia);
                    usuario.setPerfil("Usuario");
                    Long resultado = usuarioDao.insertar(usuario);
                    if(resultado>0){
                        mostrarToast("Usuario registrado");
                        onBackPressed();
                    }else{
                        mostrarToast("Hubo un problema al registrar el usuario. Intentelo nuevamente");
                    }
                }

            }
        });

    }

    private void mostrarToast(String mensaje){
        Toast.makeText(this, mensaje, Toast.LENGTH_SHORT).show();
    }
}