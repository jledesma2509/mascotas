package com.mascotas.mascotasapp;

import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.room.Room;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.mascotas.mascotasapp.data.dao.AnuncioDao;
import com.mascotas.mascotasapp.data.database.AppDatabase;
import com.mascotas.mascotasapp.databinding.FragmentAnuncioDetalleBinding;
import com.mascotas.mascotasapp.databinding.FragmentMisAnunciosCrearBinding;
import com.mascotas.mascotasapp.model.Anuncio;

import java.io.File;


public class AnuncioDetalleFragment extends Fragment {

    Anuncio anuncioUpdate;

    private FragmentAnuncioDetalleBinding binding;
    private int codigoUsuario;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_anuncio_detalle, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding = FragmentAnuncioDetalleBinding.bind(view);

        inicializar();
        adoptar();
    }

    private void adoptar() {

        binding.btnAdoptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppDatabase database = Room.databaseBuilder(getContext(), AppDatabase.class, "db-mascota")
                        .allowMainThreadQueries()
                        .build();

                AnuncioDao anuncioDao = database.getAnuncioDAO();

                int resultado = anuncioDao.adoptar(anuncioUpdate.getCodigo());

                if (resultado > 0) {
                    mostrarToast("Mascota adoptada");
                    requireActivity().onBackPressed();
                } else {
                    mostrarToast("Hubo un problema al adoptar la mascota. Intentelo nuevamente");
                }
            }
        });



    }

    private void inicializar() {

        Bundle arguments = getArguments();
        if (arguments != null) {

            anuncioUpdate = (Anuncio) arguments.getSerializable("KEY_ANUNCIO");

            binding.crearAnuncioTxtNombre.setText(anuncioUpdate.getNombre());
            binding.crearAnuncioTxtRaza.setText(anuncioUpdate.getRaza());
            binding.crearAnuncioTxtEdad.setText("" + anuncioUpdate.getEdad());
            binding.crearAnuncioTxtDescripcion.setText(anuncioUpdate.getDescripcion());

            String especie = anuncioUpdate.getEspecie();
            if (especie.equals("Perro")) {
                binding.rbPerro.setChecked(true);
            }
            else {
                binding.rbGato.setChecked(true);
            }

            if (anuncioUpdate.getImagen() != null) {
                if (!anuncioUpdate.getImagen().isEmpty()) {
                    File file = new File(anuncioUpdate.getImagen());
                    if (file.exists()) {
                        Bitmap myBitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
                        binding.crearAnuncioIvFotoMascota.setImageBitmap(myBitmap);
                    }
                }
            }

            if(anuncioUpdate.getAdoptado()==0){
                binding.btnAdoptar.setEnabled(true);
            }else{
                binding.btnAdoptar.setEnabled(false);
            }

        }

        SharedPreferences pref = getContext().getSharedPreferences("PREFERENCES_LOGIN", 0);
        codigoUsuario = pref.getInt("KEY_CODIGO", 0);
    }

    private void mostrarToast(String mensaje) {
        Toast.makeText(getContext(), mensaje, Toast.LENGTH_SHORT).show();
    }
}