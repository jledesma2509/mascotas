package com.mascotas.mascotasapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.room.Room;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.mascotas.mascotasapp.data.dao.AnuncioDao;
import com.mascotas.mascotasapp.data.database.AppDatabase;
import com.mascotas.mascotasapp.databinding.FragmentMisAnunciosBinding;
import com.mascotas.mascotasapp.model.Anuncio;

import java.util.ArrayList;
import java.util.List;


public class MisAnunciosFragment extends Fragment implements Observer<List<Anuncio>> {

    private FragmentMisAnunciosBinding binding;
    private MisAnunciosAdapter misAnunciosAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_mis_anuncios, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding = FragmentMisAnunciosBinding.bind(view);

        inicializar();
        agregarAnuncio();


    }

    private void inicializar() {
        AppDatabase database = Room.databaseBuilder(getContext(), AppDatabase.class, "db-mascota")
                .allowMainThreadQueries()
                .build();

        AnuncioDao anuncioDao = database.getAnuncioDAO();

        misAnunciosAdapter = new MisAnunciosAdapter(getContext(),new ArrayList<>());
        binding.rvMisAnuncios.setAdapter(misAnunciosAdapter);

        misAnunciosAdapter.obtenerAnuncios(new MisAnunciosAdapter.AnuncioInterface() {
            @Override
            public void onAnuncionEliminarListener(Anuncio anuncio) {

                try{
                    anuncioDao.eliminar(anuncio);
                    mostrarToast("Mascota eliminada");
                }catch (Exception ex){
                    mostrarToast("No se pudo eliminar el anuncio. Intentelo nuevamente");
                }
            }

            @Override
            public void onAnuncionEditarListener(Anuncio anuncio) {

                NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment_content_main);
                // Create the Bundle to pass, you can put String, Integer, or serializable object
                Bundle bundle = new Bundle();
                bundle.putSerializable("KEY_ANUNCIO",anuncio);
                navController.navigate(R.id.action_misAnunciosFragment2_to_misAnunciosCrearFragment, bundle);

                //Intent intent = new Intent(getContext(),MisAnunciosCrearFragment.class);
                //intent.putExtras(bundle);
                //startActivity(intent);
            }

            @Override
            public void onAnuncionDetalleListener(Anuncio anuncio) {
                NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment_content_main);
                // Create the Bundle to pass, you can put String, Integer, or serializable object
                Bundle bundle = new Bundle();
                bundle.putSerializable("KEY_ANUNCIO",anuncio);
                navController.navigate(R.id.action_misAnunciosFragment2_to_anuncioDetalleFragment, bundle);
            }
        });

        anuncioDao.obtenerAnuncios().observe(getViewLifecycleOwner(),this);

        SharedPreferences pref = getContext().getSharedPreferences("PREFERENCES_LOGIN",0);
        String perfil = pref.getString("KEY_PERFIL","");

        if(perfil.equals("Usuario")){
            binding.fabAgregarAnuncio.setVisibility(View.GONE);
        }else{
            binding.fabAgregarAnuncio.setVisibility(View.VISIBLE);
        }
    }

    private void agregarAnuncio() {

        binding.fabAgregarAnuncio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Navigation.findNavController(view).navigate(R.id.action_misAnunciosFragment2_to_misAnunciosCrearFragment);
            }
        });
    }

    private void mostrarToast(String mensaje){
        Toast.makeText(getContext(), mensaje, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onChanged(List<Anuncio> anuncios) {
        //misAnunciosAdapter = new MisAnunciosAdapter(getContext(),anuncios);
        misAnunciosAdapter.actualizarLista(anuncios);
        //binding.rvMisAnuncios.setAdapter(misAnunciosAdapter);


    }
}