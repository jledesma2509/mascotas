package com.mascotas.mascotasapp.data.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.mascotas.mascotasapp.model.Anuncio;
import com.mascotas.mascotasapp.model.Usuario;

import java.util.ArrayList;
import java.util.List;

@Dao
public interface AnuncioDao {

    @Insert()
    Long insertar(Anuncio anuncio);

    @Update
    int actualizar(Anuncio anuncio);

    @Delete
    void eliminar(Anuncio anuncio);

    @Query("select *from anuncio order by codigo desc")
    LiveData<List<Anuncio>> obtenerAnuncios();

    @Query("SELECT * FROM anuncio order by codigo desc limit 1")
    Anuncio obtenerUltimoAnuncio();

    @Query("UPDATE anuncio set adoptado = 1 where codigo=:codigo")
    int adoptar(int codigo);
}
