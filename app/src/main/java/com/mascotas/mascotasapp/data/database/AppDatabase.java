package com.mascotas.mascotasapp.data.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.mascotas.mascotasapp.data.dao.AnuncioDao;
import com.mascotas.mascotasapp.data.dao.UsuarioDao;
import com.mascotas.mascotasapp.model.Anuncio;
import com.mascotas.mascotasapp.model.Usuario;

@Database(entities = {Usuario.class, Anuncio.class}, version = 1)
public abstract class AppDatabase  extends RoomDatabase {

    public abstract UsuarioDao getUsuarioDAO();
    public abstract AnuncioDao getAnuncioDAO();
}
