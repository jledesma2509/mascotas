package com.mascotas.mascotasapp.data.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.mascotas.mascotasapp.model.Anuncio;
import com.mascotas.mascotasapp.model.Usuario;

@Dao
public interface UsuarioDao {

    @Insert()
    Long insertar(Usuario usuario);

    @Query("SELECT * FROM usuario WHERE correo = :correo")
    Usuario verificarDuplicado(String correo);

    @Query("SELECT * FROM usuario WHERE correo = :correo and contrasenia = :contrasenia")
    Usuario autenticar(String  correo, String contrasenia);
}
