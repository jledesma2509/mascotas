package com.mascotas.mascotasapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.mascotas.mascotasapp.data.dao.UsuarioDao;
import com.mascotas.mascotasapp.data.database.AppDatabase;
import com.mascotas.mascotasapp.databinding.ActivityLoginBinding;
import com.mascotas.mascotasapp.databinding.ActivityRegistrarBinding;
import com.mascotas.mascotasapp.model.Usuario;

public class LoginActivity extends AppCompatActivity {

    private ActivityLoginBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityLoginBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        inicializar();
        logIn();
        registrar();
    }

    private void inicializar() {

        SharedPreferences pref = getSharedPreferences("PREFERENCES_LOGIN",0);

        binding.emailEditText.setText(pref.getString("KEY_CORREO",""));
        binding.passwordEditText.setText(pref.getString("KEY_CLAVE",""));

        int status = pref.getInt("KEY_STATUS",0);
        if(status==1) binding.loginChkRecordar.setChecked(true);
        else binding.loginChkRecordar.setChecked(false);

    }

    private void logIn() {

        binding.loginBtnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String correo = binding.emailEditText.getText().toString();
                String clave = binding.passwordEditText.getText().toString();

                if(correo.isEmpty()){
                    mostrarToast("Debe ingresar el correo");
                    return;
                }
                if(clave.isEmpty()){
                    mostrarToast("Debe ingresar la clave");
                    return;
                }

                AppDatabase database = Room.databaseBuilder(LoginActivity.this, AppDatabase.class, "db-mascota")
                        .allowMainThreadQueries()
                        .build();

                UsuarioDao usuarioDao = database.getUsuarioDAO();

                Usuario usuario = usuarioDao.autenticar(correo,clave);

                if(usuario!=null){

                    if(binding.loginChkRecordar.isChecked()){
                        guardarPreferencia(correo,clave,1,usuario.getCodigo(),usuario.getNombres(),usuario.getPerfil());
                    }else{
                        guardarPreferencia("","",0,0,"",usuario.getPerfil());
                    }

                    Bundle bundle = new Bundle();
                    bundle.putSerializable("KEY_USUARIO",usuario);

                    Intent intent = new Intent(LoginActivity.this,MainActivity.class);
                    intent.putExtras(bundle);
                    startActivity(intent);

                }else{
                    mostrarToast("Credenciales incorrectas");
                }
            }
        });
    }

    private void registrar() {

        binding.loginLblRegistrate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this,RegistroActivity.class));
            }
        });

    }

    private void mostrarToast(String mensaje){
        Toast.makeText(this, mensaje, Toast.LENGTH_SHORT).show();
    }

    private void guardarPreferencia(String correo,String clave,int status,int codigo,String nombres,String perfil){

        SharedPreferences.Editor pref = getSharedPreferences("PREFERENCES_LOGIN",0).edit();
        pref.putString("KEY_CORREO",correo);
        pref.putString("KEY_CLAVE",clave);
        pref.putInt("KEY_STATUS",status);
        pref.putInt("KEY_CODIGO",codigo);
        pref.putString("KEY_NOMBRES",nombres);
        pref.putString("KEY_PERFIL",perfil);
        pref.apply();
    }


}