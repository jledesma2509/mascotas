package com.mascotas.mascotasapp;

import static android.app.Activity.RESULT_OK;

import android.Manifest;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.room.Room;

import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.mascotas.mascotasapp.data.dao.AnuncioDao;
import com.mascotas.mascotasapp.data.dao.UsuarioDao;
import com.mascotas.mascotasapp.data.database.AppDatabase;
import com.mascotas.mascotasapp.databinding.FragmentMisAnunciosBinding;
import com.mascotas.mascotasapp.databinding.FragmentMisAnunciosCrearBinding;
import com.mascotas.mascotasapp.model.Anuncio;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ExecutionException;


public class MisAnunciosCrearFragment extends Fragment {

    private FragmentMisAnunciosCrearBinding binding;

    String path;
    String global = "";
    String image = "";
    int codigoUsuario = 0;

    Uri photoURI;
    String mCurrentPhotoPath;
    File directorioImagenes;
    int codigoAnuncio;
    int codigo = 0;
    int indicador = 0;
    Anuncio anuncioUpdate;

    Uri imagenUri;

    private int ALL_FILES_ACCESS_PERMISSION = 3000;

    private static final int REQUEST_CODE_TAKE_PHOTO = 1000;
    int SELEC_IMAGEN = 200;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_mis_anuncios_crear, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding = FragmentMisAnunciosCrearBinding.bind(view);

        inicializar();
        crearAnuncio();
        obtenerFoto();


    }

    private void obtenerFoto() {

        binding.crearAnuncioBtnElegir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                verificarPermisos();
            }
        });

        binding.btnGaleria.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                verificarPermisosGaleria();
            }
        });
    }

    private void obtenerFotoCamara() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (takePictureIntent.resolveActivity(getContext().getPackageManager()) != null) {

            File photoFile = null;
            try {
                photoFile = crearImagen();
            } catch (IOException ex) {

            }

            if (photoFile != null) {

                ContentValues values = new ContentValues();
                values.put(MediaStore.Images.Media.TITLE, "MyPicture");
                values.put(MediaStore.Images.Media.DESCRIPTION, "Photo taken on " + System.currentTimeMillis());
                photoURI = getContext().getContentResolver().insert(
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_CODE_TAKE_PHOTO);

            }
        }
    }

    private File crearImagen() throws IOException {

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,
                ".jpg",
                storageDir
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void inicializar() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            if (!Environment.isExternalStorageManager()) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION);
                startActivityForResult(intent, ALL_FILES_ACCESS_PERMISSION);
            } else {

            }
        }

        Bundle arguments = getArguments();
        if (arguments != null) {

            anuncioUpdate = (Anuncio) arguments.getSerializable("KEY_ANUNCIO");

            binding.crearAnuncioTxtNombre.setText(anuncioUpdate.getNombre());
            binding.crearAnuncioTxtRaza.setText(anuncioUpdate.getRaza());
            binding.crearAnuncioTxtEdad.setText("" + anuncioUpdate.getEdad());
            binding.edtContacto.setText(anuncioUpdate.getContacto());
            binding.crearAnuncioTxtDescripcion.setText(anuncioUpdate.getDescripcion());
            binding.edtFechaPublicacion.setText(anuncioUpdate.getFecPublicacion());

            String especie = anuncioUpdate.getEspecie();
            if (especie.equals("Perro")) {
                binding.rbPerro.setChecked(true);
            }
            else {
                binding.rbGato.setChecked(true);
            }

            if (anuncioUpdate.getImagen() != null) {
                if (!anuncioUpdate.getImagen().isEmpty()) {
                    File file = new File(anuncioUpdate.getImagen());
                    if (file.exists()) {
                        Bitmap myBitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
                        binding.crearAnuncioIvFotoMascota.setImageBitmap(myBitmap);
                    }
                }
            }
            codigo = anuncioUpdate.getCodigo();
            indicador = 1;
        } else {
            binding.crearAnuncioTxtNombre.setText("");
            binding.crearAnuncioTxtRaza.setText("");
            binding.crearAnuncioTxtEdad.setText("");
            binding.edtContacto.setText("");
            binding.crearAnuncioTxtDescripcion.setText("");
            binding.edtFechaPublicacion.setText("");
            binding.rbPerro.setChecked(true);
        }

        SharedPreferences pref = getContext().getSharedPreferences("PREFERENCES_LOGIN", 0);
        codigoUsuario = pref.getInt("KEY_CODIGO", 0);
    }

    private void crearAnuncio() {
        binding.crearAnuncioBtnAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (codigo > 0) {
                    actualizarAnuncio();
                } else {
                    agregarAnuncio();
                }


            }
        });

    }

    private void actualizarAnuncio() {
        try {
            String nombre = binding.crearAnuncioTxtNombre.getText().toString();
            String raza = binding.crearAnuncioTxtRaza.getText().toString();
            String edad = binding.crearAnuncioTxtEdad.getText().toString();
            String descripcion = binding.crearAnuncioTxtDescripcion.getText().toString();
            String contacto = binding.edtContacto.getText().toString();
            String fecPublicacion = binding.edtFechaPublicacion.getText().toString();

            if (nombre.isEmpty()) {
                mostrarToast("Debe ingresar el nombre");
                return;
            }
            if (raza.isEmpty()) {
                mostrarToast("Debe ingresar la raza");
                return;
            }
            if (edad.isEmpty()) {
                mostrarToast("Debe ingresar la edad");
                return;
            }
            if (contacto.isEmpty()) {
                mostrarToast("Debe ingresar numero de contacto");
                return;
            }
            if (fecPublicacion.isEmpty()) {
                mostrarToast("Debe ingresar una fecha de publicacion");
                return;
            }
            if (descripcion.isEmpty()) {
                mostrarToast("Debe ingresar una descripcion");
                return;
            }

            AppDatabase database = Room.databaseBuilder(getContext(), AppDatabase.class, "db-mascota")
                    .allowMainThreadQueries()
                    .build();

            AnuncioDao anuncioDao = database.getAnuncioDAO();

            codigoAnuncio = codigo;


            if (indicador == 2) guardarImagenStorage();

            Anuncio anuncio = new Anuncio();

            anuncio.setNombre(nombre);
            anuncio.setRaza(raza);
            anuncio.setEdad(Integer.parseInt(edad));
            anuncio.setDescripcion(descripcion);
            anuncio.setContacto(contacto);
            anuncio.setFecPublicacion(fecPublicacion);
            if (indicador == 2) {
                anuncio.setImagen(directorioImagenes.getAbsolutePath());
            } else {
                anuncio.setImagen(anuncioUpdate.getImagen());
            }

            anuncio.setUsuario(codigoUsuario);
            anuncio.setCodigo(codigo);
            anuncio.setAdoptado(anuncioUpdate.getAdoptado());

            if (binding.rbPerro.isChecked()) anuncio.setEspecie("Perro");
            else anuncio.setEspecie("Gato");

            int resultado = anuncioDao.actualizar(anuncio);

            if (resultado > 0) {
                mostrarToast("Anuncio actualizado");
                requireActivity().onBackPressed();
            } else {
                mostrarToast("Hubo un problema al registrar el usuario. Intentelo nuevamente");
            }


        } catch (Exception ex) {
            mostrarToast("Hubo un problema al registrar el anuncio. Intentelo mas tarde. " + ex.getMessage());
        }
    }

    private void agregarAnuncio() {
        try {
            String nombre = binding.crearAnuncioTxtNombre.getText().toString();
            String raza = binding.crearAnuncioTxtRaza.getText().toString();
            String edad = binding.crearAnuncioTxtEdad.getText().toString();
            String descripcion = binding.crearAnuncioTxtDescripcion.getText().toString();
            String contacto = binding.edtContacto.getText().toString();
            String fecPublicacion = binding.edtFechaPublicacion.getText().toString();

            if (nombre.isEmpty()) {
                mostrarToast("Debe ingresar el nombre");
                return;
            }
            if (raza.isEmpty()) {
                mostrarToast("Debe ingresar la raza");
                return;
            }
            if (edad.isEmpty()) {
                mostrarToast("Debe ingresar la edad");
                return;
            }
            if (contacto.isEmpty()) {
                mostrarToast("Debe ingresar numero de contacto");
                return;
            }
            if (fecPublicacion.isEmpty()) {
                mostrarToast("Debe ingresar una fecha de publicacion");
                return;
            }
            if (descripcion.isEmpty()) {
                mostrarToast("Debe ingresar una descripcion");
                return;
            }


            /*if(directorioImagenes.getAbsolutePath().isEmpty()){
                mostrarToast("Debe escoger una imagen para su anuncia");
                return;
            }*/


            AppDatabase database = Room.databaseBuilder(getContext(), AppDatabase.class, "db-mascota")
                    .allowMainThreadQueries()
                    .build();

            AnuncioDao anuncioDao = database.getAnuncioDAO();

            Anuncio ultimoAnuncio = anuncioDao.obtenerUltimoAnuncio();

            if (ultimoAnuncio == null) {
                codigoAnuncio = 1;
            } else {
                codigoAnuncio = ultimoAnuncio.getCodigo() + 1;
            }

            guardarImagenStorage();

            Anuncio anuncio = new Anuncio();

            anuncio.setNombre(nombre);
            anuncio.setRaza(raza);
            anuncio.setEdad(Integer.parseInt(edad));
            anuncio.setContacto(contacto);
            anuncio.setDescripcion(descripcion);
            anuncio.setImagen(directorioImagenes.getAbsolutePath());
            anuncio.setUsuario(codigoUsuario);
            anuncio.setAdoptado(0);
            anuncio.setFecPublicacion(fecPublicacion);

            if (binding.rbPerro.isChecked()) anuncio.setEspecie("Perro");
            else anuncio.setEspecie("Gato");

            Long resultado = anuncioDao.insertar(anuncio);

            if (resultado > 0) {
                mostrarToast("Anuncio registrado");
                requireActivity().onBackPressed();
            } else {
                mostrarToast("Hubo un problema al registrar el usuario. Intentelo nuevamente");
            }


        } catch (Exception ex) {
            mostrarToast("Hubo un problema al registrar el anuncio. Intentelo mas tarde. " + ex.getMessage());
        }
    }

    public void guardarImagenStorage() {

        binding.crearAnuncioIvFotoMascota.buildDrawingCache();
        Bitmap bitmap = binding.crearAnuncioIvFotoMascota.getDrawingCache();

        OutputStream fileOutStream = null;
        Uri uri;
        try {
            File file = new File(Environment.getExternalStorageDirectory()
                    + File.separator + "imagenesguardadas" + File.separator);
            file.mkdirs();
            directorioImagenes = new File(file, codigoAnuncio + ".png");
            uri = Uri.fromFile(directorioImagenes);
            fileOutStream = new FileOutputStream(directorioImagenes);
        } catch (Exception e) {
            Log.e("ERROR!", e.getMessage());
        }
        try {
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fileOutStream);
            fileOutStream.flush();
            fileOutStream.close();
        } catch (Exception e) {
            Log.e("ERROR!", e.getMessage());
        }
    }

    private void verificarPermisos() {


        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.CAMERA)) {
            } else {
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.CAMERA},
                        1);
            }
        } else if (ContextCompat.checkSelfPermission(getContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {


            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

            } else {
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        2);
            }
        } else {
            obtenerFotoCamara();
        }
    }

    private void verificarPermisosGaleria() {


        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            } else {
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        1);
            }
        } else if (ContextCompat.checkSelfPermission(getContext(),
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {


            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {

            } else {
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        2);
            }
        } else {
            seleccionarImagen();
        }
    }

    private void seleccionarImagen() {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(intent,SELEC_IMAGEN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE_TAKE_PHOTO && resultCode == RESULT_OK) {

            Bitmap bitmap;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), photoURI);
                binding.crearAnuncioIvFotoMascota.setImageBitmap(bitmap);
                if (indicador == 1) {
                    indicador = 2;
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            /*if (requestCode == REQUEST_CODE_TAKE_PHOTO && resultCode == RESULT_OK) {
                Bundle extras = data.getExtras(); // Aquí es null
                Bitmap imageBitmap = (Bitmap) extras.get("data");
                mPhotoImageView.setImageBitmap(imageBitmap);
            }*/

        }else if(requestCode == SELEC_IMAGEN && resultCode == RESULT_OK){

            imagenUri = data.getData();
            binding.crearAnuncioIvFotoMascota.setImageURI(imagenUri);
            if (indicador == 1) {
                indicador = 2;
            }
            /*ConvertBitmapToString convertirBitmap = new ConvertBitmapToString();
            convertirBitmap.execute(bitmap);

            try {
                image = convertirBitmap.get();

            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }*/
        }

    }

    /*private void obtenerFotoCamara() {
        String nombreImagen = "";
        File fileImagen = getContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        boolean isCreada = fileImagen.exists();

        if(!isCreada) {
            isCreada = fileImagen.mkdirs();
        }

        if(isCreada) {
            nombreImagen = (System.currentTimeMillis() / 1000) + ".jpg";
        }

        path = getContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES)+File.separator+nombreImagen;
        File imagen = new File(path);

        Intent intent = null;
        intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            String authorities = getContext().getPackageName()+".provider";
            Uri imageUri = FileProvider.getUriForFile(getContext(), authorities, imagen);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        } else {
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(imagen));
        }


        startActivityForResult(intent, 1000);
    }*/

    /*@Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == Activity.RESULT_OK && requestCode == 1000) {
            MediaScannerConnection.scanFile(getContext(), new String[]{path}, null, new MediaScannerConnection.OnScanCompletedListener() {
                @Override
                public void onScanCompleted(String s, Uri uri) {

                }
            });

            Bitmap bitmap = BitmapFactory.decodeFile(path);
            binding.crearAnuncioIvFotoMascota.setImageBitmap(bitmap);
            ConvertBitmapToString convertirBitmap = new ConvertBitmapToString();
            convertirBitmap.execute(bitmap);

            try {
                image = convertirBitmap.get();

            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }*/



    private void mostrarToast(String mensaje) {
        Toast.makeText(getContext(), mensaje, Toast.LENGTH_SHORT).show();
    }
}