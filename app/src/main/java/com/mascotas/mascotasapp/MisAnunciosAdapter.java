package com.mascotas.mascotasapp;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.mascotas.mascotasapp.model.Anuncio;

import java.io.File;
import java.util.List;

public class MisAnunciosAdapter extends RecyclerView.Adapter<MisAnunciosAdapter.MisAnunciosAdapterViewHolder>{


    Context context;
    List<Anuncio> anuncios;
    AnuncioInterface listener;

    public MisAnunciosAdapter(Context context,List<Anuncio> anuncios) {

        this.context = context;
        this.anuncios = anuncios;
    }

    public void actualizarLista(List<Anuncio> anuncios){
        this.anuncios = anuncios;
        notifyDataSetChanged();
    }

    @Override
    public MisAnunciosAdapter.MisAnunciosAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_mis_anuncios, parent, false);
        return new MisAnunciosAdapter.MisAnunciosAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MisAnunciosAdapter.MisAnunciosAdapterViewHolder holder, int position) {

        final Anuncio anuncio = anuncios.get(position);

        holder.tvNombre.setText(anuncio.getNombre());
        holder.tvTipo.setText(anuncio.getEspecie());
        holder.tvEdad.setText(""+anuncio.getEdad());
        holder.tvRaza.setText(anuncio.getRaza());
        holder.tvPreferencia.setText(anuncio.getDescripcion());
        holder.tvContacto.setText(anuncio.getContacto());
        holder.tvfecPublicacion.setText(anuncio.getFecPublicacion());


        if(anuncio.getImagen()!=null){
            if(!anuncio.getImagen().isEmpty()){
                File file = new File(anuncio.getImagen());

                if(file.exists()){
                    Bitmap myBitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
                    holder.imgMascota.setImageBitmap(myBitmap);

                }
            }
        }

        SharedPreferences pref = context.getSharedPreferences("PREFERENCES_LOGIN",0);
        String perfil = pref.getString("KEY_PERFIL","");

        if(perfil.equals("Usuario")){
            holder.imgEliminar.setVisibility(View.GONE);
            holder.imgEditar.setVisibility(View.GONE);
            holder.imgDetalle.setVisibility(View.VISIBLE);
        }else{
            holder.imgEliminar.setVisibility(View.VISIBLE);
            holder.imgEditar.setVisibility(View.VISIBLE);
            holder.imgDetalle.setVisibility(View.GONE);
        }

        if(anuncio.getAdoptado()==0){
            holder.imgAdoptado.setVisibility(View.GONE);
        }else{
            holder.imgAdoptado.setVisibility(View.VISIBLE);
        }


        holder.imgEliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onAnuncionEliminarListener(anuncio);
            }
        });
        holder.imgEditar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onAnuncionEditarListener(anuncio);
            }
        });
        holder.imgDetalle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onAnuncionDetalleListener(anuncio);
            }
        });
    }

    public interface AnuncioInterface{
        void onAnuncionEliminarListener(Anuncio anuncio);
        void onAnuncionEditarListener(Anuncio anuncio);
        void onAnuncionDetalleListener(Anuncio anuncio);
    }


    public void obtenerAnuncios(AnuncioInterface listener){
        this.listener = listener;
    }

    @Override
    public int getItemCount() {
        return anuncios.size();
    }

    public class MisAnunciosAdapterViewHolder extends RecyclerView.ViewHolder {


        TextView tvNombre,tvRaza,tvTipo,tvEdad,tvPreferencia,tvContacto,tvfecPublicacion;
        ImageView imgMascota,imgEliminar,imgEditar,imgDetalle,imgAdoptado;

        public MisAnunciosAdapterViewHolder(View itemView) {
            super(itemView);

            tvNombre = itemView.findViewById(R.id.tvNombre);
            tvRaza = itemView.findViewById(R.id.tvRaza);
            tvTipo = itemView.findViewById(R.id.tvTipo);
            tvEdad = itemView.findViewById(R.id.tvEdad);
            tvPreferencia = itemView.findViewById(R.id.tvPreferencia);
            imgMascota = itemView.findViewById(R.id.imgMascota);
            imgEliminar = itemView.findViewById(R.id.imgEliminar);
            imgEditar = itemView.findViewById(R.id.imgEditar);
            imgDetalle = itemView.findViewById(R.id.imgDetalle);
            imgAdoptado = itemView.findViewById(R.id.imgAdoptado);
            tvContacto = itemView.findViewById(R.id.tvContacto);
            tvfecPublicacion = itemView.findViewById(R.id.tvfecPublicacion);


        }
    }
}